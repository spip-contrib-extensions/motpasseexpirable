# Mots de passe expirables

![motpasseexpirable](./prive/themes/spip/images/motpasseexpirable-xx.svg)

Force les auteurs à renouveler leur mot de passe régulièrement.

## Documentation

https://contrib.spip.net/4425
